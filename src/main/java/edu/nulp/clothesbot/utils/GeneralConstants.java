package edu.nulp.clothesbot.utils;

import java.util.Arrays;
import java.util.List;

public interface GeneralConstants {

    String USER_AGENT_MOZILLA = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; en-us) " +
                                "AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5";

    String DIVIDER = "/";
    String DASH_SIGN = "-";
    String PLUS_SIGN = "+";
    String PARAMETER_DIVIDER = "&";
    String QUESTION_MARK = "?";


    List<String> ACTION_MESSAGES = Arrays.asList(
            "What are you looking for?",
            "Tell me what should I search now!"
    );

    List<String> ERROR_MESSAGES = Arrays.asList(
            "I’m sorry, I didn’t catch what you said.",
            "I didn’t understand that :( Would you mind repeating it?",
            "Oh, looks like I couldn't find anything, could you change your request, please?"
    );

    String WELCOME_MESSAGE = "Hi %s, I'm CloBot. You can search for clothes with my help. " +
                             "Just send me what are you looking for and I'll try to " +
                             "find something.\nBut first of all tell me please how many results do " +
                             "you want to receive (1 - 9)?";

    String RESULT_COUNT_MESSAGE = "Please enter the number from 1 to 9 to describe how many result you " +
                                  "want to receive.";

    String GENDER_ASK_MESSAGE = "Also I need to know you gender.";

    String START_COMMAND = "/start";

}
