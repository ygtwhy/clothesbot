package edu.nulp.clothesbot.utils;

public class LinkBuilder {

    private StringBuilder url = new StringBuilder();

    public LinkBuilder(String url) {
        this.url.append(url)
                .append(GeneralConstants.DIVIDER);
    }

    public LinkBuilder addPath(String path) {
        this.url.append(path)
                .append(GeneralConstants.DIVIDER);

        return this;
    }

    public String build() {
        return this.url.toString();
    }

}
