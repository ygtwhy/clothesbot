package edu.nulp.clothesbot.utils;

import edu.nulp.clothesbot.pojo.Item;

public class DataUtils {

    public static final Item createItem(String imgUrl, String itemUrl, String itemName) {
        Item adidasItem = new Item();
        adidasItem.setImgUrl(imgUrl);
        adidasItem.setUrl(itemUrl);
        adidasItem.setName(itemName);

        return adidasItem;
    }

}
