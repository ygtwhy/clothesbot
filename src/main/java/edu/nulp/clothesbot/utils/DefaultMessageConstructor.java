package edu.nulp.clothesbot.utils;

import edu.nulp.clothesbot.pojo.UserSex;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.Arrays;
import java.util.Random;

import static edu.nulp.clothesbot.utils.GeneralConstants.*;

public class DefaultMessageConstructor {


    public static SendMessage getActionMessage(Long chatId) {
        String actionMsg = ACTION_MESSAGES.get(new Random().nextInt(ACTION_MESSAGES.size()));

        return createMessage(chatId, actionMsg);
    }

    public static SendMessage getErrorMessage(Long chatId) {
        String errorMsg = ERROR_MESSAGES.get(new Random().nextInt(ERROR_MESSAGES.size()));

        return createMessage(chatId, errorMsg);
    }

    public static SendMessage getSexEnterMessage(Message message) {
        SendMessage sendMessage = createMessage(message.getChatId(), GENDER_ASK_MESSAGE);

        KeyboardRow keyboardRow = new KeyboardRow();
        keyboardRow.addAll(Arrays.asList(UserSex.MALE.getSex(), UserSex.FEMALE.getSex()));

        ReplyKeyboardMarkup replyMarkup = new ReplyKeyboardMarkup();
        replyMarkup.setOneTimeKeyboard(true);
        replyMarkup.getKeyboard().add(keyboardRow);
        sendMessage.setReplyMarkup(replyMarkup);

        return sendMessage;
    }

    public static SendMessage createMessage(Long chatId, String msg) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(msg);

        return sendMessage;
    }

}
