package edu.nulp.clothesbot.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.dialogflow.v2.Dialogflow;
import com.google.api.services.dialogflow.v2.DialogflowScopes;
import com.google.api.services.dialogflow.v2.model.GoogleCloudDialogflowV2DetectIntentRequest;
import com.google.api.services.dialogflow.v2.model.GoogleCloudDialogflowV2DetectIntentResponse;
import com.google.api.services.dialogflow.v2.model.GoogleCloudDialogflowV2QueryInput;
import com.google.api.services.dialogflow.v2.model.GoogleCloudDialogflowV2TextInput;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import edu.nulp.clothesbot.pojo.Query;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.GeneralSecurityException;

@Component
public class DialogFlowConnector {

    public static final String AGENT_KEY_PATH = "/agentkey.json";
    public static final String APPLICATION_NAME = "CloBot";
    public static final String AGENT_ID = "newagent-f1fb4";
    public static final String SESSION_PATTERN = "projects/%s/agent/sessions/%s";

    public static final String JSON_QUERY_RESULT = "queryResult";
    public static final String JSON_PARAMETERS = "parameters";

    private Dialogflow dialogflow;

    public DialogFlowConnector() {
        try {
            HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            GoogleCredential credential = GoogleCredential
                    .fromStream(DialogFlowConnector.class.getResourceAsStream(AGENT_KEY_PATH))
                    .createScoped(DialogflowScopes.all());

            credential.refreshToken();

            this.dialogflow = new Dialogflow.Builder(httpTransport, jsonFactory, credential)
                    .setApplicationName(APPLICATION_NAME)
                    .build();

        } catch (IOException | GeneralSecurityException e) {
            e.printStackTrace();
        }
    }

    public Query getResult(String text, String languageCode, Integer userId) {
        GoogleCloudDialogflowV2TextInput textInput = new GoogleCloudDialogflowV2TextInput();
        textInput.setLanguageCode(languageCode);
        textInput.setText(text);

        GoogleCloudDialogflowV2QueryInput queryInput = new GoogleCloudDialogflowV2QueryInput();
        queryInput.setText(textInput);

        GoogleCloudDialogflowV2DetectIntentRequest detectIntentRequest =
                new GoogleCloudDialogflowV2DetectIntentRequest();
        detectIntentRequest.setQueryInput(queryInput);

        String session = String.format(SESSION_PATTERN, AGENT_ID, userId);

        try {
            Dialogflow.Projects.Agent.Sessions.DetectIntent detectIntent = dialogflow
                    .projects().agent().sessions().detectIntent(session, detectIntentRequest);

            GoogleCloudDialogflowV2DetectIntentResponse detectIntentResponse = detectIntent.execute();

            return parseResponse(detectIntentResponse.toString());
        } catch (IOException e) {
            e.printStackTrace();
            return new Query();
        }

    }

    private Query parseResponse(String response) {
        Gson gson = new Gson();
        JsonObject jsonResponse = new JsonParser().parse(response).getAsJsonObject();

        if (jsonResponse.has(JSON_QUERY_RESULT)) {
            JsonObject jsonQueryResult = jsonResponse.getAsJsonObject(JSON_QUERY_RESULT);

            if (jsonQueryResult.has(JSON_PARAMETERS)) {
                JsonObject jsonParameters = jsonQueryResult.getAsJsonObject(JSON_PARAMETERS);

                return gson.fromJson(jsonParameters, Query.class);
            }
        }

        return new Query();
    }

}
