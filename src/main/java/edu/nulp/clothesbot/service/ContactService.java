package edu.nulp.clothesbot.service;

import edu.nulp.clothesbot.model.UserSettings;
import edu.nulp.clothesbot.pojo.UserSex;
import edu.nulp.clothesbot.repository.UserSettingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactService {

    @Autowired
    private UserSettingRepository userSettingRepository;

    public UserSettings getUserSettings(Integer userId) {
        return userSettingRepository.findByUserId(userId);
    }

    public void saveUserResultCount(Integer userId, Integer resultCount) {
        UserSettings userSettings = userSettingRepository.findByUserId(userId);

        if (userSettings == null) {
            userSettings = new UserSettings();
            userSettings.setUserId(userId);
        }

        userSettings.setResultCount(resultCount);
        userSettingRepository.save(userSettings);
    }

    public void saveUserSex(Integer userId, UserSex userSex) {
        UserSettings userSettings = userSettingRepository.findByUserId(userId);

        if (userSettings != null) {
            userSettings.setSex(userSex);
            userSettingRepository.save(userSettings);
        }
    }

}
