package edu.nulp.clothesbot.service.adidas;

import edu.nulp.clothesbot.model.UserSettings;
import edu.nulp.clothesbot.pojo.Item;
import edu.nulp.clothesbot.api.IItemService;
import edu.nulp.clothesbot.utils.DataUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("adidasItemService")
public class AdidasItemService implements IItemService {

    @Override
    public List<Item> getItemsAccordingToUserSettings(String html, Integer maxResultCount) throws RuntimeException {
        List<Item> adidasItems = new ArrayList<>();

        Document doc = Jsoup.parse(html);
        Elements elems = doc.select("div[data-auto-id='product_container']")
                .first()     //first and only element
                .children(); // 48 child elements, we need only first 4
        Element itemInfos = elems.get(0).child(0);

        int itemsCount = itemInfos.children().size() > maxResultCount ? maxResultCount : itemInfos.children().size();

        for (int i = 0; i < itemsCount; i++) {
            Element itemInfo = itemInfos.child(i);

            if (itemInfo == null) {
                break;
            }

            String imgUrl = itemInfo.select("img[data-auto-id='image']")
                    .first()
                    .attr("src"); // https://assets.adidas.com/images/w_280,h_280,f_auto,q_auto:sensitive,fl_lossy/0659db2d1c1e43a9a6b0a92a01344985_9366/pharrell-williams-solarhu-nmd-shoes.jpg

            Elements itemDetails = itemInfo.select("div.gl-product-card__details");

            String itemUrl = itemDetails.select("a") //  /us/pharrell-williams-solarhu-nmd-shoes/BB9531.html?forceSelSize=11
                    .first()
                    .attr("href");

            String itemName = itemDetails.select("div.gl-product-card__details-main")
                    .select("[title]")
                    .attr("title");

            adidasItems.add(DataUtils.createItem(imgUrl, itemUrl, itemName));
        }

        return adidasItems;
    }

}
