package edu.nulp.clothesbot.service;

import edu.nulp.clothesbot.api.IItemService;
import edu.nulp.clothesbot.api.IMessageDispatcher;
import edu.nulp.clothesbot.api.adidas.AdidasConstants;
import edu.nulp.clothesbot.api.adidas.AdidasSearcher;
import edu.nulp.clothesbot.pojo.Item;
import edu.nulp.clothesbot.pojo.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MessageDispatcher implements IMessageDispatcher {

    @Autowired
    private AdidasSearcher adidasSearcher;

    @Autowired
    private IItemService adidasItemService;

    @Override
    public List<SendPhoto> dispatch(Long chatId, Query query) throws RuntimeException {
        String pageHtmlResult = this.adidasSearcher
                .getPageHtmlResult(query.getGarment(), query.getSize(), query.getColor(), query.getUserSex());

        List<Item> items = adidasItemService
                .getItemsAccordingToUserSettings(pageHtmlResult, query.getMaxResultCount());

        return items.stream()
                .map(item -> createItemWithPhoto(chatId, item))
                .collect(Collectors.toList());
    }

    private SendPhoto createItemWithPhoto(Long chatId, Item item) {
        SendPhoto sendPhoto = new SendPhoto()
                .setChatId(chatId);

        try {
            URL imageURL = new URL(item.getImgUrl());
            URLConnection conn = imageURL.openConnection();
            InputStream in = conn.getInputStream();

            InlineKeyboardButton keyboardButton = new InlineKeyboardButton();
            keyboardButton.setUrl(AdidasConstants.DEFAULT_URL + item.getUrl());
            keyboardButton.setText(item.getName());
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            inlineKeyboardMarkup.getKeyboard().add(Collections.singletonList(keyboardButton));

            sendPhoto.setPhoto(item.getName(), in)
                     .setReplyMarkup(inlineKeyboardMarkup);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sendPhoto;
    }

}
