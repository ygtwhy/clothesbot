package edu.nulp.clothesbot.service;

import edu.nulp.clothesbot.api.IMessageDispatcher;
import edu.nulp.clothesbot.api.IMessageHandler;
import edu.nulp.clothesbot.model.UserSettings;
import edu.nulp.clothesbot.pojo.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.ArrayList;
import java.util.List;

@Component
public class MessageHandler implements IMessageHandler {

    public static final String DEFAULT_LANGUAGE_CODE = "en";

    @Autowired
    private IMessageDispatcher messageDispatcher;

    @Autowired
    private DialogFlowConnector dialogFlowConnector;

    @Override
    public List<SendPhoto> handle(Message message, UserSettings userSettings) throws Exception {
        List<SendPhoto> botApiMethod = new ArrayList<>();

        String text = message.getText();
        Integer userId = message.getFrom().getId();

        if (text != null) {
            Query query = dialogFlowConnector.getResult(text, DEFAULT_LANGUAGE_CODE, userId);
            query.setUserSex(userSettings.getSex());
            query.setMaxResultCount(userSettings.getResultCount());

            if (query.getGarment() == null) {
                throw new Exception("No garment is set!");
            }

            botApiMethod = messageDispatcher.dispatch(message.getChatId(), query);
        }

        return botApiMethod;
    }

}
