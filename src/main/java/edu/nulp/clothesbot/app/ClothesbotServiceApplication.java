package edu.nulp.clothesbot.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication(scanBasePackages={"edu.nulp.clothesbot"})
@EntityScan(basePackages = { "edu.nulp.clothesbot.model" })
@EnableJpaRepositories("edu.nulp.clothesbot.repository")
public class ClothesbotServiceApplication {

	@Autowired
	private BotContext botContext;

	public static void main(String[] args) {
		ApiContextInitializer.init();

		SpringApplication.run(ClothesbotServiceApplication.class, args);
	}

}
