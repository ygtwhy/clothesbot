package edu.nulp.clothesbot.app;

import edu.nulp.clothesbot.telegram.CloBot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

import javax.annotation.PostConstruct;

@Component
public class BotContext {

    @Autowired
    private CloBot cloBot;

    @PostConstruct
    public void init() {
        TelegramBotsApi botsApi = new TelegramBotsApi();

        try {
            botsApi.registerBot(this.cloBot);
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }

}
