package edu.nulp.clothesbot.api.adidas;

public interface AdidasConstants {

    String DEFAULT_URL = "https://www.adidas.com";
    String DEFAULT_LANGUAGE = "/us";

    String MEN_PATH = "men-";
    String WOMEN_PATH = "women-";

    String SHOES_PATH = "shoes";
    String T_SHIRTS_PATH = "short_sleeve_shirts";
    String PANTS_PATH = "pants";
    String HOODIES_SWEATSHIRTS_PATH = "hoodies_sweatshirts";

    String US_SIZE_ATTRIBUTE = "v_size_en_us";

}
