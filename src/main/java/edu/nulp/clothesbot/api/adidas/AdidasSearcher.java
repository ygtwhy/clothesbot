package edu.nulp.clothesbot.api.adidas;

import edu.nulp.clothesbot.pojo.UserSex;
import edu.nulp.clothesbot.utils.GeneralConstants;
import edu.nulp.clothesbot.utils.HttpUtils;
import edu.nulp.clothesbot.utils.LinkBuilder;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

@Service
public class AdidasSearcher {

    private HttpClient client;

    public AdidasSearcher() {
        this.client = HttpUtils.createHttpClient();
    }

    public String getPageHtmlResult(String garment, String size, String color, UserSex userSex) {
        StringBuilder result = new StringBuilder();

        try {
            String garmentPath = getGarmentPath(garment, color, userSex);

            String url = new LinkBuilder(AdidasConstants.DEFAULT_URL)
                    .addPath(AdidasConstants.DEFAULT_LANGUAGE)
                    .addPath(garmentPath)
                    .build();

            URIBuilder uriBuilder = new URIBuilder(url);

            if (!StringUtils.isEmpty(size)) {
                uriBuilder.setParameter(AdidasConstants.US_SIZE_ATTRIBUTE, size);
            }

            HttpGet request = new HttpGet(uriBuilder.build());
            HttpResponse response = client.execute(request);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        return result.toString();
    }

    private String getGarmentPath(String garment, String color, UserSex sex) {
        String sexPath = convertSex(sex);
        String garmentPath = convertGarment(garment);

        StringBuilder sb = new StringBuilder()
                .append(sexPath);

        if (!StringUtils.isEmpty(color)) {
            sb.append(color)
              .append(GeneralConstants.DASH_SIGN);
        }

        sb.append(garmentPath);
        return sb.toString();
    }

    private String convertSex(UserSex sex) {
        return UserSex.MALE == sex ? AdidasConstants.MEN_PATH : AdidasConstants.WOMEN_PATH;
    }

    private String convertGarment(String garment) {
        switch (garment) {
            case "t-shirt" : return AdidasConstants.T_SHIRTS_PATH;
            case "pants" : return AdidasConstants.PANTS_PATH;
            case "shoes" : return AdidasConstants.SHOES_PATH;
            case "sneakers" : return AdidasConstants.SHOES_PATH;
            case "sweatshirt" : return AdidasConstants.HOODIES_SWEATSHIRTS_PATH;
            case "hoodie" : return AdidasConstants.HOODIES_SWEATSHIRTS_PATH;
        }

        return "";
    }


}
