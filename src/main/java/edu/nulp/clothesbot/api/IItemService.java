package edu.nulp.clothesbot.api;

import edu.nulp.clothesbot.pojo.Item;

import java.util.List;

public interface IItemService {

    List<Item> getItemsAccordingToUserSettings(String html, Integer maxResultCount) throws RuntimeException;

}
