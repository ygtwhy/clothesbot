package edu.nulp.clothesbot.api;

import edu.nulp.clothesbot.pojo.Query;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;

import java.util.List;

public interface IMessageDispatcher {

    List<SendPhoto> dispatch(Long chatId, Query query) throws Exception;

}
