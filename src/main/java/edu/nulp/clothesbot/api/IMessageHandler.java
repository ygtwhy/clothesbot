package edu.nulp.clothesbot.api;

import edu.nulp.clothesbot.model.UserSettings;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.List;

public interface IMessageHandler {

    List<SendPhoto> handle(Message message, UserSettings userSettings) throws Exception;

}
