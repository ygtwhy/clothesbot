package edu.nulp.clothesbot.repository;

import edu.nulp.clothesbot.model.UserSettings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSettingRepository extends JpaRepository<UserSettings, Long> {

    UserSettings findByUserId(Integer userId);

}
