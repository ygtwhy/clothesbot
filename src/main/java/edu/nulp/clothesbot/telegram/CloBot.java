package edu.nulp.clothesbot.telegram;

import edu.nulp.clothesbot.api.IMessageHandler;
import edu.nulp.clothesbot.model.UserSettings;
import edu.nulp.clothesbot.pojo.UserSex;
import edu.nulp.clothesbot.service.ContactService;
import edu.nulp.clothesbot.utils.DefaultMessageConstructor;
import edu.nulp.clothesbot.utils.GeneralConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.List;

import static edu.nulp.clothesbot.utils.GeneralConstants.RESULT_COUNT_MESSAGE;
import static edu.nulp.clothesbot.utils.GeneralConstants.WELCOME_MESSAGE;

@Component
public class CloBot extends TelegramLongPollingBot {

    @Autowired
    private IMessageHandler messageHandler;

    @Autowired
    private ContactService contactService;

    @Override
    @ExceptionHandler(value = TelegramApiException.class)
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            try {
                Message message = update.getMessage();

                if (GeneralConstants.START_COMMAND.equals(message.getText())) {
                    handleStartMessage(message);
                    return;
                }

                User from = message.getFrom();
                UserSettings userSettings = contactService.getUserSettings(from.getId());

                if (userSettings == null) {
                    handleResultCountEntering(message);
                } else if (userSettings.getSex() == null) {
                    handleSexEntering(message);
                } else {
                    List<SendPhoto> botApiMethods = messageHandler.handle(message, userSettings);

                    for (SendPhoto sendPhoto : botApiMethods) {
                        execute(sendPhoto);
                    }
                }

            } catch (Exception exception) {
                exception.printStackTrace();
                handleException(update);
            }
        }
    }

    @Override
    public String getBotUsername() {
        return "clothzbot";
    }

    @Override
    public String getBotToken() {
        return "776193272:AAFD8s4PAQ7GgIFudr7f-J73pYIYIVRRuwk";
    }

    private void handleStartMessage(Message message) throws TelegramApiException {
        String msg = String.format(WELCOME_MESSAGE, message.getFrom().getFirstName());
        SendMessage sendMessage = DefaultMessageConstructor.createMessage(message.getChatId(), msg);

        execute(sendMessage);
    }

    private void handleResultCountEntering(Message message) throws TelegramApiException {
        String msg = message.getText();

        if (StringUtils.isNumeric(msg)) {
            Integer count = Integer.parseInt(msg);

            if (1 <= count && count < 10) {
                contactService.saveUserResultCount(message.getFrom().getId(), count);
                SendMessage sendMessage = DefaultMessageConstructor.getSexEnterMessage(message);
                execute(sendMessage);
                return;
            }
        }

        SendMessage sendMessage = DefaultMessageConstructor.createMessage(message.getChatId(), RESULT_COUNT_MESSAGE);
        execute(sendMessage);
    }

    private void handleSexEntering(Message message) throws TelegramApiException {
        UserSex sex = UserSex.parse(message.getText());

        if (sex != null) {
            contactService.saveUserSex(message.getFrom().getId(), sex);
            execute(DefaultMessageConstructor.getActionMessage(message.getChatId()));
        } else {
            SendMessage sendMessage = DefaultMessageConstructor.getSexEnterMessage(message);
            execute(sendMessage);
        }
    }

    private void handleException(Update update) {
        SendMessage sendMessage = DefaultMessageConstructor.getErrorMessage(update.getMessage().getChatId());

        try {
            execute(sendMessage);
        } catch (TelegramApiException telegramException) {
            telegramException.printStackTrace();
        }
    }
}
