package edu.nulp.clothesbot.pojo;

public enum UserSex {
    MALE("m", "Male", "men"),
    FEMALE("f", "Female", "women");

    private String sexShort;
    private String sex;
    private String sexDescription;

    UserSex(String sexShort, String sex, String sexDescription) {
        this.sexShort = sexShort;
        this.sex = sex;
        this.sexDescription = sexDescription;
    }

    public static UserSex parse(String input) {
        if (MALE.sex.equals(input)) {
            return MALE;
        } else if (FEMALE.sex.equals(input)) {
            return FEMALE;
        }

        return null;
    }

    public String getSexShort() {
        return this.sexShort;
    }

    public String getSexDescription() {
        return this.sexDescription;
    }

    public String getSex() {
        return this.sex;
    }

}
